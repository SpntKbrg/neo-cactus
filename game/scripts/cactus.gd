extends RigidBody2D

export var life = 3
export var angle_lock = 0.25 * PI
var falling = true

func _ready():
  set_process(true)

func _process(delta):
  if (falling == true):
    self.set_collision_mask_bit(3, false)
    self.set_collision_layer_bit(3, false)

func hit():
  life = life - 1
  if (life < 0):
    queue_free()

func _on_cactus_body_entered( body ):
  if(body.get_name()=="water" and falling == true):
    falling = false
    self.set_collision_layer_bit(3, true)
    self.set_collision_mask_bit(3, true)
extends RigidBody2D

func _ready():
  pass

func _on_raindrop_body_entered( body ):
  if(body.get_name()=="water"):
    queue_free()
  if(body.get_name()=="cactus_plant"):
    body.get_owner().hit()
    queue_free()
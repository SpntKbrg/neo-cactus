extends Node2D

export var delay = 3
export var edge = 100.0
export var max_cac = 3
export (PackedScene) var cactus

var total = 0
var timer = 0

func _ready():
  set_process(true)


func _process(delta):
  total = get_child_count()
  if (total < max_cac) :
    timer = timer + delta
    if (timer >= delay):
      spawn_cac()
      timer = 0

func spawn_cac():
  var c = cactus.instance()
  c.add_to_group("cactus")
  add_child(c)
  c.set_position(Vector2(rand_range(0 + edge, get_viewport().get_visible_rect().size.x - edge),-10))
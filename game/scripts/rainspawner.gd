extends Node2D

export var delay = 0.3
export var edge = 10.0
export (PackedScene) var raindrop

var timer = 0

func _ready():
  set_process(true)


func _process(delta):
  timer = timer + delta
  if (timer >= delay):
    timer = 0
    spawn_rain()

func spawn_rain():
  var c = raindrop.instance()
  c.add_to_group("rain")
  add_child(c)
  c.set_position(Vector2(rand_range(0 + edge, get_viewport().get_visible_rect().size.x) - edge,-10))

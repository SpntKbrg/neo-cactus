extends KinematicBody2D

export var speed = 10.0

func _ready():
  set_process(true)

func _process(delta):
  self.set_position(Vector2(self.get_position().x + speed, self.get_position().y))
  if (self.get_position().x > get_viewport_rect().size.x or self.get_position().x < -get_viewport_rect().size.x):
    speed = - speed
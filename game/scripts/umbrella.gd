extends KinematicBody2D

export var speed = 5.0
export var edge = 40.0
export var margin = 80.0
var water_level = 30.0
export (String) var go_left
export (String) var go_right
export (String) var go_up
export (String) var go_down

func _ready():
  set_process(true)

func _process(delta):
  if (self.get_position().x > self.get_viewport_rect().size.x - edge):
    self.set_position(Vector2(self.get_viewport_rect().size.x - edge, self.get_position().y))
  elif (self.get_position().x < edge):
    self.set_position(Vector2(edge, self.get_position().y))
  elif (Input.is_action_pressed(go_left)):
    self.set_position(Vector2(self.get_position().x - speed, self.get_position().y))
  elif (Input.is_action_pressed(go_right)):
    self.set_position(Vector2(self.get_position().x + speed, self.get_position().y))
  if (self.get_position().y > self.get_viewport_rect().size.y - water_level - margin):
    self.set_position(Vector2(self.get_position().x, self.get_viewport_rect().size.y - water_level - margin))
  elif (self.get_position().y < margin):
    self.set_position(Vector2(self.get_position().x, margin))
  elif (Input.is_action_pressed(go_up)):
    self.set_position(Vector2(self.get_position().x, self.get_position().y - speed))
  elif (Input.is_action_pressed(go_down)):
    self.set_position(Vector2(self.get_position().x, self.get_position().y + speed))
